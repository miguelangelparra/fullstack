/*************************************SUBMIT_1*********************************************/
console.log("Starting javascript...");

//Exercise 1
var name = 'Miguelangel Parra';
console.log('My name is: ' + name);

//Exercise 2
var age = 26;
console.log('My age is: ' + age);

//Exercise 3
var ignasiAge = 32;
var ageDiff = ignasiAge - age;
console.log('The different between my age and Ignasi\'age is: ' + ageDiff);

//Exercise 4
if (age > 21) {
	console.log('You are older than 21');
} else {
	console.log('You are not older than 21');
}

//Exercise 5
if (age > ignasiAge) {
	console.log('Ignasi is younger than you');
} else if (age < ignasiAge) {
	console.log('Ignasi is older than you');
} else {
	console.log('You have the same age as Ignasi');
}
/* Como el codigo es funcional para lo solicitado me pareció
innecesario agregar la expresion que evalua la condicion de
igualdad con == o ===*/
