/*************************************SUBMIT_1*********************************************/
console.log("Starting javascript...");

//Exercise 1
var name = 'Miguelangel Parra';
console.log('My name is: ' + name);

//Exercise 2
var age = 26;
console.log('My age is: ' + age);

//Exercise 3
var ignasiAge = 32;
var ageDiff = ignasiAge - age;
console.log('The different between my age and Ignasi\'age is: ' + ageDiff);

//Exercise 4
if (age > 21) {
	console.log('You are older than 21');
} else {
	console.log('You are not older than 21');
}

//Exercise 5
if (age > ignasiAge) {
	console.log('Ignasi is younger than you');
} else if (age < ignasiAge) {
	console.log('Ignasi is older than you');
} else {
	console.log('You have the same age as Ignasi');
}
/* Como el codigo es funcional para lo solicitado me pareció
innecesario agregar la expresion que evalua la condicion de
igualdad con == o ===*/

/*******************************SUBMIT_2***************************************/
/*****************************JavaScript Array Functions**********************/
/*Exercise 1*/
var namesOfClassmate = [
	'Leonardo Sanchez',
	'Gaston Gonzalez',
	'Agus',
	'Andrea Briceño',
	'Antonella Pansserini',
	'Branko Haberkon',
	'Camila Gauna',
	'Claudio Pera',
	'David Cosio',
	'Diego Weinmann',
	'Gemny Ibarra',
	'Martha Barzola',
	'Miguelangel Parra',
	'Nicolas Higa',
	'Silvia Nesci',
	'Valeria Fernandez'
];
console.log('**************************************************');
console.log('********Classmate List Disordered*****');
console.log('*******************************');
console.log('Classmate List disordered: ' + namesOfClassmate);

namesOfClassmate.sort();

console.log('**************************************************');
console.log('****First Name of Classmate List******');
console.log('*******************************');
console.log('First name of Classmate List: ' + namesOfClassmate[0]);

console.log('**************************************************');
console.log('*******Last Name of Classmate List****');
console.log('*******************************');
console.log('Last name of Classmate List: ' + namesOfClassmate[namesOfClassmate.length - 1]);

console.log('**************************************************');
console.log('********Classmate List Sorted*********');
console.log('*******************************');
console.log('Classmate List sorted: ' + namesOfClassmate);

console.log('**************************************************');
console.log('***Classmate List Sorted with FOR*****');
console.log('*******************************');

for (var i = 0; i < namesOfClassmate.length; i++){
	console.log('Classmate number(' + i + '): ' + namesOfClassmate[i])
};

/*Exercise 2:*/
var ageClassmate = [
	23,
	43,
	34,
	28,
	12,
	25,
	21,
	36,
	48,
	67,
	43,
	21,
	12,
	26,
	23,
	36,
	23,
];

console.log('**************************************************');
console.log('**********Age List with while*********');
console.log('*******************************');
var w = 0
while (w < namesOfClassmate.length) {
	console.log('Classmate number(' + w + ') has : ' + ageClassmate[w] + ' years old.');
	++w;
}

console.log('**************************************************');
console.log('**Age List: Print ONLY EVEN NUMBERS***');
console.log('*******************************');
var w = 0
while (w < namesOfClassmate.length) {
	if(ageClassmate[w] % 2 == 0){
		console.log('Classmate number('+ w +') has : ' + ageClassmate[w] + ' years old.');
	}
	++w;
}

console.log('**************************************************');
console.log('**Age List with FOR: Print ONLY EVEN NUMBERS***');
console.log('*******************************');
for (z=0; z < namesOfClassmate.length; z++) {
	if(ageClassmate[z] % 2 == 0){
		console.log('Classmate number(' + z + ') has : ' + ageClassmate[z] + ' years old.');
	};
}

//Exercise 3
console.log('**************************************************');
console.log('**********The lowest age**************');
console.log('*******************************');
function minimum(arrMinimum){
	return console.log('The lowest age is: ' + Math.min(...arrMinimum));
}
minimum(ageClassmate);

//Exercise 4
console.log('**************************************************');
console.log('************The biggest age**************');
console.log('*******************************');
function maximun(arrMaximum){
	return console.log('The biggest age is: ' + Math.max(...arrMaximum));
}
maximun(ageClassmate);

//Exercise 5
console.log('**************************************************');
console.log('******Return value of a index************');
console.log('*******************************');
function index (arr, index){
    return console.log('The value of index('+index+') is: '+arr[index]);
}
index(ageClassmate,1);

//Exercise 6
console.log('**************************************************');
	console.log('******The Values that repeat************');
	console.log('*******************************');
	var arrB= [3,6,67,6,23,11,100,8,93,0,17,24,7,1,33,45,28,33,23,12,99,100];
	var arrC= [3,6,67,6,23,11,100,8,93,0,17,24,7,1,33,45,28,33,23,12,99,100,6,6,6,800];

	function valuesRepeat(arr){
		var arrAux = [];

	for (i=0; i<arr.length; i++){
		for(j=i+1; j<arr.length;j++){
			if(arr[i]==arr[j] && arrAux.includes(arr[i])==false){
				arrAux.push(arr[i])
			}
		}
	}
		console.log(arrAux)
	};
	valuesRepeat(arrB)
	valuesRepeat(arrC)

//Exercise 7
console.log('**************************************************');
console.log('******Array to String************');
console.log('*******************************');
myColor = ["Red", "Green", "White", "Black"];
function arrToString(arr){
	return console.log(arr.join(','));
}
arrToString(myColor);

/*******************************SUBMIT_3***************************************/
/*****************************JavaScript Strings*******************************/
/*Exercise 1*/
console.log('**************************************************');
console.log('******Reverse***************************');
console.log('*******************************');
function reverse(number){
	var numberToString= number.toString();
	var arrVacio=[]
	for (var i = numberToString.length-1;i>-1;i--){
		arrVacio.push(numberToString[i])
	};
	return console.log(arrVacio);
}
reverse(22332);

/*Exercise 2*/
console.log('**************************************************');
console.log('******Alphabetical Order******************');
console.log('*******************************');
function alphabetical(words){
	arrVacio=words.split("")
	return console.log(arrVacio.sort());
}
alphabetical("caleidoscopio");

/*Exercise 3*/
console.log('**************************************************');
console.log('************Uppercase******************');
console.log('*******************************');
function uppercase(words){
	var arr= words.split(" ")
	var arrVacio = arr.map((word)=>{
		return word.charAt(0).toUpperCase() + word.slice(1)
	})
	return console.log(arrVacio.join(" "))
}
uppercase("Esta funcion recibe palabras y cada primera letra es transformada en mayuscula");

/*Exercise 4*/
console.log('**************************************************');
console.log('************Longest word******************');
console.log('*******************************');
function longestword(phrase,a){
	var arr= phrase.split(" ")
	var arrVacio= arr.reduce((longest,word)=>{
		if (longest.length>word.length){
			return longest
		} else{
			return word
		}},0)
		return console.log(arrVacio)
	}
	longestword("Esta funcion recibe palabras y cada primera letra es transformada en mayuscula");
