
//Headers (Datos para la solicitud de datos al servidor)
var init = {
  method: 'GET',
  headers: { 'X-API-Key': 'uWXbbuTY8jRbLvkqFPmD39gtSXXRKTJ0mXCBPnxw' }
}

//Solicitud de datos al servidor
fetch(url, init)
  .then(response => response.json())
  .then((responseJson) => { app1.members = responseJson.results[0].members })
  .catch(() => alert("Lo sentimos, tuvimos problema con el servidor. (Vuelve más tarde!)"))

//Instancia de VUE
var app1 = new Vue({
  el: '#vueDiv',

  data: {
    //Datos crudos de miembros
    members: [],

    //Variables para filtrar  (con valores inicializados)
    selectedState: "allStates",
    selectedParties: ["R", "D", "I"],
  },

  computed: {
    //Dibuja estados en el select
    states() { return Array.from(new Set(this.members.map(member => member.state))).sort() },

    //Filtra a los miembros por estado y partido
    toFilteredMembers() {
      return this.members.filter(member =>
        (this.selectedState == member.state || this.selectedState == "allStates")
        && (this.selectedParties.includes(member.party)))
    }
  }
})
