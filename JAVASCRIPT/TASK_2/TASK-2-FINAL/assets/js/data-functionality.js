
var dataRaw = data.results[0].members


//DRAWS

var tBody = document.querySelector("#table-data > tbody") //To call TBODY
function toDrawTable(memberToDraw) {                     //To Draw table
  tBody.innerHTML = " "   // To clear table

  //TBODY
  memberToDraw.forEach((member) => {         //To create tr for each member
    var tr = document.createElement("tr");
    tBody.appendChild(tr);

    member.forEach((dataMember) => {       		//To create td for each data'member
      var td = document.createElement("td");
      td.innerHTML = dataMember
      tr.appendChild(td)
    })
  })
}

function toDrawStates() {      //To draw states into optionmenu
  var selectState = document.querySelector("#stateOption")
  var states = []

  function getStatesFromDataRaw() {      //To get states from  DataRaw
    [data.results[0].members].map((member) => {
      return member.map((dataMember) => {
        if (states.includes(dataMember.state) == false) {
          return states.push(dataMember.state)
        }
      })
    })
  }
  getStatesFromDataRaw()

  states.forEach((state) => {         //To create and draw states
    var optionstate = document.createElement("option");
    optionstate.innerText = state
    optionstate.value = state
    selectState.appendChild(optionstate);
  })
}

//GETS
function getCheckboxesValues() {     // To get checkboxes values
  var nodeListsCheckboxes = document.querySelectorAll('input[name=checkboxes]:checked')
  var arrayCheckboxes = Array.from(nodeListsCheckboxes)
  var checkboxesValues = arrayCheckboxes.map(checkbox => checkbox.value)
  return checkboxesValues
}
function getStateValue() {	//To get state value
  return $("#stateOption").val() || $("#stateOption").val() == "allStates"
}

//FILTERS
function toFilter() {                                  //To filter DataRaw
  var allMembers = dataRaw.map((valor) => [
    '<a href="' + valor.url + '">' + valor.last_name + ' ' + valor.first_name + (valor.middle_name || '') + '</a>',
    valor.party,
    valor.state,
    valor.seniority + " years",
    valor.votes_with_party_pct + "%"])

  var membersFilteredByStates = allMembers.filter((a) => {  // To filter by state
    var stateValue = getStateValue()
    if (stateValue == "allStates") { return (a) }
    else if (stateValue == a[2]) {
      return (a)
    }
  })

  var membersFilteredByPartiesAndStates = membersFilteredByStates.filter((a) => {  // to filter by parties and state
    var valuesParties = getCheckboxesValues()
    if (valuesParties.includes(a[1])) {
      return (a)
    }
  })

  return toDrawTable(membersFilteredByPartiesAndStates)
}

//CALLS
toDrawStates() // To call DrawStates
toFilter()   //To call filter function first time

//EVENTS
document.getElementsByName('checkboxes').forEach((checkbox) => { //To listener Event from parties'checkboxes                
  checkbox.addEventListener('change', toFilter)
})

document.getElementById('stateOption').addEventListener('change', toFilter) // To listener Event from stateOptions












//Alternaiva para dibujar el TH
/*function toDrawThead() {     // To draw THEAD
  var theadValues = ["CONGRESSMAN", "PARTY", "STATE", "SENIORITY", "VOTES"]
  theadValues.forEach((th) => {
    var thead = document.querySelector("table > thead > tr")
    var td = document.createElement("td")
    td.innerText = th;
    thead.appendChild(td)
  })
}

toDrawThead()  //To call drawTHead
*/


