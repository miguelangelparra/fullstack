  
var dataRaw = data.results[0].members


//DRAWS

function toDrawTable(memberToDraw, idTable) {                     //To Draw table
  var tBody = document.querySelector("[id=" + CSS.escape(idTable) + "]" + " > tbody") //To call TBODY
  tBody.innerHTML = " "   // To clear table
  //TBODY
  memberToDraw.forEach((member) => {         //To create tr for each member
    var tr = document.createElement("tr");
    tBody.appendChild(tr);

    member.forEach((dataMember) => {       		//To create td for each data'member
      var td = document.createElement("td");
      td.innerHTML = dataMember
      tr.appendChild(td)
    })
  })
}

function toDrawStates() {      //To draw states into optionmenu
  var selectState = document.querySelector("#stateOption")
  var states = []

  function getStatesFromDataRaw() {      //To get states from  DataRaw
    [data.results[0].members].map((member) => {
      return member.map((dataMember) => {
        if (states.includes(dataMember.state) == false) {
          return states.push(dataMember.state)
        }
      })
    })
  }
  getStatesFromDataRaw()

  states.forEach((state) => {         //To create and draw states
    var optionstate = document.createElement("option");
    optionstate.innerText = state
    optionstate.value = state
    selectState.appendChild(optionstate);
  })
}

//GETS
function getCheckboxesValues() {     // To get checkboxes values
  var nodeListsCheckboxes = document.querySelectorAll('input[name=checkboxes]:checked')
  var arrayCheckboxes = Array.from(nodeListsCheckboxes)
  var checkboxesValues = arrayCheckboxes.map(checkbox => checkbox.value)
  return checkboxesValues
}
function getStateValue() {	//To get state value
  return $("#stateOption").val() || $("#stateOption").val() == "allStates"
}

//FILTERS
function toFilter() {                                  //To filter DataRaw
  var allMembers = dataRaw.map((valor) => [
    '<a href="' + valor.url + '">' + valor.last_name + ' ' + valor.first_name + (valor.middle_name || '') + '</a>',
    valor.party,
    valor.state,
    valor.seniority + " years",
    valor.votes_with_party_pct + "%"])

  var membersFilteredByStates = allMembers.filter((member) => {  // To filter by state
    var stateValue = getStateValue()
    if (stateValue == "allStates") { return (member) }
    else if (stateValue == member[2]) {
      return (member)
    }
  })

  var membersFilteredByPartiesAndStates = membersFilteredByStates.filter((member) => {  // to filter by parties and state
    var valuesParties = getCheckboxesValues()
    if (valuesParties.includes(member[1])) {
      return (member)
    }
  })

  return toDrawTable(membersFilteredByPartiesAndStates, "table-data")
}

//CALLS
toDrawStates() // To call DrawStates
toFilter()   //To call filter function first time

//EVENTS
document.getElementsByName('checkboxes').forEach((checkbox) => { //To listener Event from parties'checkboxes                
  checkbox.addEventListener('change', toFilter)
})

document.getElementById('stateOption').addEventListener('change', toFilter) // To listener Event from stateOptions





