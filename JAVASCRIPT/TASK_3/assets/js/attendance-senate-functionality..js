

var dataRaw = data.results[0].members

function toDrawTable(memberToDraw, idTable) {                     //To Draw table
	var tBody = document.querySelector("[id=" + CSS.escape(idTable) + "]" + " > tbody") //To call TBODY
	//TBODY
	memberToDraw.forEach((member) => {         //To create tr for each member
		var tr = document.createElement("tr");
		tBody.appendChild(tr);

		member.forEach((dataMember) => {       		//To create td for each data'member
			var td = document.createElement("td");
			td.innerHTML = dataMember
			tr.appendChild(td)
		})
	})
}

function getMembersByParty(partyOfMember) {
	var members = dataRaw.filter(member => member.party == partyOfMember)
	return members
}

function getAverageVotesParty(membersOfParty) {
	return (membersOfParty.reduce((accumulator, member) => accumulator + member.votes_with_party_pct, 0) / membersOfParty.length).toFixed(2)
}

var republicants = getMembersByParty("R")
var democrats = getMembersByParty("D")
var independents = getMembersByParty("I")

statistics.numbers_of_republicans = republicants.length
statistics.numbers_of_democrats = democrats.length
statistics.numbers_of_independents = independents.length
statistics.numbers_of_total = dataRaw.length

statistics.average_votes_party_republicants = getAverageVotesParty(republicants)
statistics.average_votes_party_democrats = getAverageVotesParty(democrats)
statistics.average_votes_party_independents = getAverageVotesParty(independents)
statistics.average_votes_party_total = getAverageVotesParty(dataRaw)

statistics.least_often_vote_with_their_party = toFilterByPercentil(dataRaw, 10, ">", "votes_with_party_pct")
statistics.most_often_vote_with_their_party = toFilterByPercentil(dataRaw, 90, "", "votes_with_party_pct")
statistics.most_engaged = (toFilterByPercentil(dataRaw, 10, ">", "missed_votes_pct"))
statistics.least_engaged = toFilterByPercentil(dataRaw, 90, "", "missed_votes_pct").reverse()

function toFilterByPercentil(data, percentilSearched, type, variable) { //Se utilizó la logica de los percentiles en estadistica
	let dataOrdered = data.sort((a, b) => (a[variable] - b[variable])) //Ordena los datos segun el porcentaje de la variable indicada en parametro
	let positionPercentil = Math.ceil((percentilSearched * dataOrdered.length) / 100) - 1 // busca el entero superior de la posicion del percentil indicado
	let valueOfPositionByPercentil = dataOrdered[positionPercentil][variable] //busca el valor del porcentaje de la posicion del percentil indicado
	let dataFilteredByPercentil = dataOrdered.filter((member) => (type == ">" ? member[variable] <= valueOfPositionByPercentil : member[variable] >= valueOfPositionByPercentil)) //Filtra segun el valor de la posicion en la cola o en la punta del array
	return dataFilteredByPercentil
}

//Data a imprimir:
var SenateAtAGlance = [
	["<b>Democrats</b>", statistics.numbers_of_democrats, statistics.average_votes_party_democrats],
	["<b>Republicans</b>", statistics.numbers_of_republicans, statistics.average_votes_party_republicants],
	["<b>Independents</b>", statistics.numbers_of_independents, statistics.average_votes_party_independents],
	["<b>Total</b>", statistics.numbers_of_total, statistics.average_votes_party_total]]
toDrawTable(SenateAtAGlance, "senateAtAGlance")


var tableLeastEngaged = statistics.least_engaged.map((member) => [
	'<a href="' + member.url + '">' + member.last_name + ' ' + member.first_name + (member.middle_name || '') + '</a>',
	member.party,
	member.missed_votes,
	member.missed_votes_pct + "%",
])
toDrawTable(tableLeastEngaged, "leastEngaged")

var tableMostEngaged = statistics.most_engaged.map((member) => [
	'<a href="' + member.url + '">' + member.last_name + ' ' + member.first_name + (member.middle_name || '') + '</a>',
	member.party,
	member.missed_votes,
	member.missed_votes_pct + "%",
])
toDrawTable(tableMostEngaged, "mostEngaged")



console.log(statistics)

