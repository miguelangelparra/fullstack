$(function () {
    loadData();
});


var arrLocation = [];
//var arrHorizontal = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"];
//var arrVertical = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J"];




var dates = document.querySelectorAll('*[id^="B_"]');
dates.forEach(s => {
    s.setAttribute("ondrop", "drop(event)");
    s.setAttribute("ondragover", "allowDrop(event)")
})


function posicionamiento(orientacion, posicionIni, tipo) {
    arrConverPosicionIni = posicionIni.toString().split("")

    let numPosicion;
    let numConservado;
    if (orientacion){
        numPosicion= arrConverPosicionIni[2]
        numConservado = arrConverPosicionIni[3]
    }else{
        numPosicion= arrConverPosicionIni[3]
        numConservado= arrConverPosicionIni[2]
        

    }
    
    let arrAuxLocation = []
    switch (tipo) {
        case "Aircraft":
            posicion(5);
            break;
        case "Battleship":
            posicion(4)
            break;
        case "Submarine":
            posicion(3)
            break;
        case "Destroyer":
            posicion(3)
            break;
        case "Patrol":
            posicion(2);
            break;
    }

    function posicion(cantidad) {

        if(parseInt(numPosicion) + cantidad > 10){alert("Posicion Prohibida"); return}

        for (let i = parseInt(numPosicion); i < parseInt(numPosicion) + cantidad; i++) {
            let posicionFinal;
            if (orientacion) {
                posicionFinal = i + numConservado

            }
            else {
                posicionFinal = numConservado + i

            }
            arrAuxLocation.push(posicionFinal);
        }

    }
    arrLocation.push({ type: tipo, locations: arrAuxLocation })
    console.log(arrLocation);
}

function drop(ev) {
    ev.preventDefault();
    console.log("Comenzó el dropeo")
    var data = ev.dataTransfer.getData("text");
    console.log(ev.target.id)
    ev.target.appendChild(document.getElementById(data));
    posicionamiento(false,ev.target.id,data)
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}
function allowDrop(ev) {
    ev.preventDefault();

    console.log("Fue dropeado el elemento")
}



function getParameterByName(name) {
    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
};

function loadData() {
    $.get('/api/game_view/' + getParameterByName('gp'))
        .done(function (data) {
            var playerInfo;
            if (data.gamePlayers.length == 1) {
                playerInfo = [data.gamePlayers[0].player]
            }
            else if (data.gamePlayers[0].id == getParameterByName('gp')) {
                playerInfo = [data.gamePlayers[0].player, data.gamePlayers[1].player];
            }
            else
                playerInfo = [data.gamePlayers[1].player, data.gamePlayers[0].player];

            $('#userLogged').text('It´s time to win, ' + playerInfo[0].email)
            var player2 = playerInfo[1] != undefined ? playerInfo[1].email : ""
            $('#playerInfo').text(playerInfo[0].email + '(you) vs ' + player2);

            data.ships.forEach(function (shipPiece) {
                shipPiece.locations.forEach(function (shipLocation) {
                    if (isHit(shipLocation, data.salvoes, playerInfo[0].id) != 0) {
                        $('#B_' + shipLocation).addClass('ship-piece-hited');
                        $('#B_' + shipLocation).text(isHit(shipLocation, data.salvoes, playerInfo[0].id));
                    } else
                        $('#B_' + shipLocation).addClass('ship-piece');
                });
            });
            data.salvoes.forEach(function (salvo) {
                if (playerInfo[0].id === salvo.player) {
                    salvo.locations.forEach(function (location) {
                        $('#S_' + location).addClass('salvo-piece');
                    });
                } else {
                    salvo.locations.forEach(function (location) {
                        $('#B_' + location).addClass('salvo');
                    });
                }
            });
        })

        .fail(function (jqXHR, textStatus) {
            alert("Failed: " + textStatus);
        });
};

function isHit(shipLocation, salvoes, playerId) {
    var turn = 0;
    salvoes.forEach(function (salvo) {
        if (salvo.player != playerId)
            salvo.locations.forEach(function (location) {
                if (shipLocation === location)
                    turn = salvo.turn;
            });
    });
    return turn;
}

function toLogOut() {
    $.post("/api/logout").done(function () {
        console.log("logged out");
        location.href = "/web/games.html"
    })
}


function toAddShips() {
    $.post(
        {
            url: '/api/games/players/' + getParameterByName('gp') + '/ships',
            data: JSON.stringify(arrLocation),
            dataType: "text",
            contentType: "application/json"
        })
        .done(function (data) {
            console.log("success");
            location.reload()
        })
        .fail(function (jqXHR, textStatus) {
            console.log(jqXHR.status)
        })
}

/*[{ typeShip: "destroyer", locations: ["A1", "B1", "C1"] },
            { typeShip: "patrol boat", locations: ["H5", "H6"] }
            ])*/